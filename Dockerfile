FROM docker.io/alpine:3.16

RUN apk --no-cache add xvfb metacity

COPY entrypoint.sh /entrypoint.sh

CMD ["/entrypoint.sh"]
EXPOSE 6099/tcp
